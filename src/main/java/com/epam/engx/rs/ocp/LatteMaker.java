package com.epam.engx.rs.ocp;


public class LatteMaker extends AbstractMaker {

    public Cup make() {
        Cup cup = takeNewEmptyCup();
        cup.add("espresso");
        cup.add("milk");
        cup.add("milk");
        cup.add("foam");
        return cup;
    }
}